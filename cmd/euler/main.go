package main

import "gitlab.com/charliebritton/go-euler/cmd/euler/cmd"

func main() {
	cmd.Execute()
}
