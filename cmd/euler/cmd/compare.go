package cmd

import (
	"crypto/sha256"
	"fmt"

	"github.com/spf13/cobra"
	euler "gitlab.com/charliebritton/go-euler/problems"
)

func init() {
	rootCmd.AddCommand(compareCmd)
}

var compareCmd = &cobra.Command{
	Use:   "compare <problem> <answer>",
	Short: "Compares a given answer to the correct answer for the problem",
	Args:  cobra.ExactArgs(2),
	RunE:  compare,
}

// Compares supplied result to correct result.
func compare(cmd *cobra.Command, args []string) error {

	prob, err := euler.RetreiveProblem(args[0])
	if err != nil {
		return err
	}

	actual := []byte(prob.ResultHash)
	your := sha256.Sum256([]byte(args[1]))

	if actual != your {
		fmt.Printf("sha256 of your answer:    %x\nsha256 of correct answer: %x\nPlease try again!", your, actual)
		return nil
	}

	fmt.Printf("Your answer of %d is correct! Well done!", args[1])
	return nil
}
