package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	euler "gitlab.com/charliebritton/go-euler/problems"
)

func init() {
	rootCmd.AddCommand(infoCmd)
}

var infoCmd = &cobra.Command{
	Use:   "info <problem>",
	Short: "Print information for problem x of Project Euler",
	Args:  cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {

		prob, err := euler.RetreiveProblem(args[0])
		if err != nil {
			return err
		}
		fmt.Printf(`Here's problem %d!
Link: https://projecteuler.net/overview=%03d
		
Description:
%s

Answer Hash: %v (sha256)
(compare your answer with euler compare %d <your answer>)
`, prob.ID, prob.ID, prob.Description, prob.ResultHash, prob.ID)

		return nil
	},
}
