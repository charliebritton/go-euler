package cmd

import "github.com/spf13/cobra"

var rootCmd = &cobra.Command{
	Use:   "euler",
	Short: "Euler is a package implementing various Project Euler problems",
}

// Execute runs the rootCmd
func Execute() error {
	return rootCmd.Execute()
}
