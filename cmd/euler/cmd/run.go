package cmd

import (
	"crypto/sha256"
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	euler "gitlab.com/charliebritton/go-euler/problems"
)

// RawOutput Output answer as raw value?
var RawOutput bool

func init() {
	rootCmd.AddCommand(runCmd)
	runCmd.PersistentFlags().BoolVarP(&RawOutput, "raw", "r", false, "raw output")
}

var runCmd = &cobra.Command{
	Use:   "run <problem> [--raw]",
	Short: "Runs my implementation of the selected problem and returns the sha256 hash or raw output if specified with the --raw flag.",
	Args:  cobra.ExactArgs(1),
	RunE:  run,
}

// Runs my implementation of the problem and returns the shasum of the result
// unless raw output has wbeen selected.
func run(cmd *cobra.Command, args []string) error {

	prob, err := euler.RetreiveProblem(args[0])
	if err != nil {
		return err
	}

	fmt.Printf("Computing answer for problem %d! This may take a few seconds...\n", prob.ID)
	a := prob.Function()
	if !RawOutput {
		fmt.Printf("sha256: %x\n", sha256.Sum256([]byte(strconv.Itoa(a))))
	} else {
		fmt.Printf("Answer: %d\n", a)
	}

	return nil

}
