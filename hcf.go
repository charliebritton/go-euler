package euler

// TODO Integer overflow errors

// HighestCommonFactor takes two integers and returns the highest common factor
// of them. This assumes that hcf(a, 0) = 0
func HighestCommonFactor(a int, b int) (int, error) {

	var temp int

	// Handle Zero Case
	if a <= 0 || b <= 0 {
		return 0, nil
	}

	// Handle Same Number
	if a == b {
		return a, nil
	}

	for b > 0 {

		temp = b
		b = a % b
		a = temp

	}

	return a, nil

}

// HCFGroup takes a slice of integers and returns the highest common factor of
// them collectively.
func HCFGroup(s []int) (int, error) {

	var hcf = s[0]
	for _, n := range s[1:] {

		res, err := HighestCommonFactor(hcf, n)
		if err != nil {
			return 0, err
		}
		hcf = res
	}
	return hcf, nil

}
