package euler

// ProblemTwo runs the second project euler problem
func ProblemTwo() (res int) {

	var next int
	var prev int

	// Starting conditions
	prev = 1

	for i := 2; i < 4000000; i = next {

		// Satisfy even values only
		if i%2 == 0 {

			res += i

		}

		next = i + prev
		prev = i

	}
	return
}
