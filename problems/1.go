package euler

// ProblemOne computes the answer for problem 1
func ProblemOne() (res int) {

	for i := 1; i < 1000; i++ {

		if i%3 == 0 || i%5 == 0 {

			res += i

		}

	}
	return

}
