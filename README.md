# `go-euler`
This is my implementation of the algorithms needed to solve problems on
[Project Euler](https://projecteuler.net). The generalised algorithms can be
found in the base directory of this project. Implementations for the specific
problems are in the `problems` directory and can be run using the `euler`
command.

This repository will be updated every time I solve a problem.

## Features
- CLI for Project Euler problems
- Works offline
- Answer comparison function
- Full coverage benchmarks
- Full coverage testing

## Structure
The main library with the general helper functions and their tests is located at
the root of the project. This is part of the euler library. Specific problems
are located in the `problems` directory, under the problem number. Within each
`problem` file, there is a function such as `ProblemOne` that takes no arguments
but returns an integer. This means that the problem can then be referenced from
the `problems/info.go` file, which acts as the database for the library.

The `cmd/euler` directory holds the code for the `euler` command and each
subcommand has its own file, e.g. `compare.go` for the comparison function. See
the [Cobra Documentation](https://github.com/spf13/cobra/README.md) for a more
thorough guide on that.

## Command Usage
```
go-euler info    <problem>
go-euler compare <problem> <your answer>
go-euler run     <problem> [-r] [--raw]
```
This runs my implementation of the problem and returns the sha256sum of the
answer. You can check your answer by passing it in as an argument and the
program will compare yours to the correct answer.

## Why?
- I'm writing implementations of the problems in Go to help me better understand
the language
- I wanted a command line tool so I could use Project Euler offline
- I wanted to have some open source code!

## Contributing
Please feel free to submit a pull request if you can improve the
readability/execution time of my code through a benchmark. Make sure the modified
version passes all tests!

### Run Tests
```
go test -run=.
```

### Run Tests & Benchmarks
```
go test -bench=.
```

## License & Credit
All source code in this package is licensed under [**CC BY-NC-SA 4.0**](LICENSE).
The problems in this repository come from [Project Euler](https://projecteuler.net).

Copyright Notice from projecteuler.net/copyright:

	Copyright Information
	The content on the site is licenced under a Creative Commons Licence:

	Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
	Creative Commons License
	You can read a Summary of the Licence, but that is not a substitute for the Full Licence Legal Code.

	Can I make use of the problems elsewhere?
	Yes! It is not only granted but it is encouraged for material to be shared and used freely for non-profit making purposes. But please read on...

	All of the problems have been gathered from a variety of sources: some are based on interesting sub-results of larger mathematical theorems, others are variations of classic problems, the rest are believed to be original. The primary mission of Project Euler is to present high quality problems for the purpose of both encouraging learning and to challenge in an enjoyable context.


	Are there any restrictions?
	Considerable effort and thought has been put into the design of each problem, and in that sense all material is new and original. For this reason problems must not be used for commercial purposes.


	Do I need to show attribution?
	Under the CC Licence it is a requirement, but it should also be a matter of courtesy. You are entirely at liberty to use the material as it is or adapt it. If you choose to use the content as it appears on the website, then you could say something like, "The following problem is taken from Project Euler." If you modify the problem then you could say something like, "The following problem was inspired by Problem xxx at Project Euler." But please note that the terms of the licence require that any derivative work will be subject to the same licence.


	Do you have a preferred way of linking directly to a particular problem?
	You can link directly to Problem xxx with the following URI:

	https://projecteuler.net/problem=xxx

Please see the [LICENSE](LICENSE) file for full legal text.