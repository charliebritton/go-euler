package euler

import "testing"

func Test_absInt(t *testing.T) {
	type args struct {
		n int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		// Zero
		{
			"Zero",
			args{0},
			0,
		},
		// Positive Numbers
		{
			"One",
			args{1},
			1,
		},
		{
			"Less than int32",
			args{2147483646},
			2147483646,
		},
		{
			"Less than int64",
			args{9223372036854775807},
			9223372036854775807,
		},
		// Negative Numbers
		{
			"Minus One",
			args{-1},
			1,
		},
		{
			"Greater than minus int32",
			args{-2147483646},
			2147483646,
		},
		{
			"Greater than minus int64",
			args{-9223372036854775807},
			9223372036854775807,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := absInt(tt.args.n); got != tt.want {
				t.Errorf("absInt() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLowestCommonMultiple(t *testing.T) {
	type args struct {
		a int
		b int
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr bool
	}{
		{
			"Zero and Zero",
			args{0, 0},
			0,
			true,
		},
		{
			"Zero and Positive",
			args{0, 12345},
			0,
			false,
		},
		{
			"Zero and Negative",
			args{0, -12345},
			0,
			false,
		},
		{
			"Positive + Positive",
			args{25565234, 36423432},
			465586781081544,
			false,
		},
		// Pos + Pos
		// Pos same
		// Pos + Neg
		// Pos plus same neg
		// Neg + Neg
		// Neg + same neg
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := LowestCommonMultiple(tt.args.a, tt.args.b)
			if (err != nil) != tt.wantErr {
				t.Errorf("LowestCommonMultiple() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("LowestCommonMultiple() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLCMGroup(t *testing.T) {
	type args struct {
		n []int
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := LCMGroup(tt.args.n)
			if (err != nil) != tt.wantErr {
				t.Errorf("LCMGroup() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("LCMGroup() = %v, want %v", got, tt.want)
			}
		})
	}
}
