package euler

import (
	"testing"
)

func TestReverse(t *testing.T) {
	type args struct {
		x int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"Reverse 123456789", args{123456789}, 987654321},
		{"Reverse 2147483647", args{2147483647}, 7463847412},
		{"Reverse 3147483647", args{3147483647}, 7463847413},
		{"Reverse 9223372036854775807", args{9223372036854775807}, 7085774586302733229},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Reverse(tt.args.x); got != tt.want {
				t.Errorf("Reverse() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLargestPalindromicProduct(t *testing.T) {
	type args struct {
		n int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"Zero", args{0}, 0},
		{"One", args{1}, 9},
		{"Two", args{2}, 9009},
		{"Three", args{3}, 906609},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := LargestPalindromicProduct(tt.args.n); got != tt.want {
				t.Errorf("One() = %v, want %v", got, tt.want)
			}
		})
	}
}

var exponent = 7

func BenchmarkLPP(b *testing.B) {

	for i := 0; i < b.N; i++ {
		LargestPalindromicProduct(exponent)
	}

}
