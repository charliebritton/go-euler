package euler

import (
	"testing"
)

func TestHighestCommonFactor(t *testing.T) {
	type args struct {
		a int
		b int
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr bool
	}{
		{
			"Same Numbers",
			args{
				13, 13,
			},
			13,
			false,
		},
		{
			"Different Primes",
			args{
				13, 19,
			},
			1,
			false,
		},
		{
			"One Prime",
			args{
				37, 600,
			},
			1,
			false,
		},
		{
			"One Multiple of Other",
			args{
				20, 100,
			},
			20,
			false,
		},
		{
			"Straight Case",
			args{
				624129, 2061517,
			},
			18913,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := HighestCommonFactor(tt.args.a, tt.args.b)
			if (err != nil) != tt.wantErr {
				t.Errorf("HighestCommonFactor() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("HighestCommonFactor() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHCFGroup(t *testing.T) {
	type args struct {
		s []int
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr bool
	}{
		{
			"Three Numbers",
			args{
				[]int{184, 230, 276},
			},
			46,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := HCFGroup(tt.args.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("HCFGroup() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("HCFGroup() = %v, want %v", got, tt.want)
			}
		})
	}
}

var n = []int{
	184, 230, 276,
}

func BenchmarkHighestCommonFactor(b *testing.B) {

	b.Run("Two Numbers", func(B *testing.B) {

		for i := 0; i < b.N; i++ {
			HighestCommonFactor(624129, 2061517)
		}

	})

	b.Run("Three Numbers", func(B *testing.B) {

		for i := 0; i < b.N; i++ {
			HCFGroup(n)
		}

	})

}
