package euler

import (
	"math"
)

// One takes an integer and returns its largest factor.
// func One(n int) int {

// 	var lf = 1
// 	var f = 2

// 	for n > 1 {
// 		if n%f == 0 {
// 			lf = f
// 			n = n / f
// 			f = 2
// 			continue
// 		}
// 		f++
// 	}
// 	return lf
// }

// Two takes an integer and returns its largest factor.
// func Two(n int) int {

// 	var f, lf int

// 	// Special case for 2
// 	f = 2
// 	if n%f == 0 {
// 		n = n / 2
// 		lf = 2

// 		for n%f == 0 {
// 			n = n / 2
// 		}
// 	} else {
// 		lf = 1
// 	}

// 	// 3, 5, 7, 9, ...
// 	f = 3
// 	for n > 1 {
// 		if n%f == 0 {
// 			n = n / f
// 			lf = f

// 			for n%f == 0 {
// 				n = n / f
// 			}
// 		}
// 		f += 2
// 	}
// 	return lf
// }

// Three version 3
func LargestPrimeFactor(n int) int {

	var f, lf int
	var mf int

	f = 2
	if n%f == 0 {

		n = n / 2
		lf = 2

		for n%f == 0 {
			n = n / 2
		}
	} else {
		lf = 1
	}

	f = 3
	mf = int(math.Floor(math.Sqrt(float64(n))))
	for n > 1 && f <= mf {
		if n%f == 0 {

			n = n / f
			lf = f

			for n%f == 0 {
				n = n / f
			}

			// Max factor can only be at most √n
			mf = int(math.Ceil(math.Sqrt(float64(n))))

		}
		f += 2
	}

	if n == 1 {
		return lf
	}

	return n

}

// func main() {

// 	// v1
// 	// Benchmark time: 117399 ns/op

// 	// Whilst n > 1, loop following
// 	// f = 2
// 	// n % f == 0? If so store f as lf to persist and set n = n/f
// 	// f = 2
// 	// n % f != 0? Increase f by 1

// 	// Run One
// 	//res := One(600851475143)

// 	// ===============
// 	// v2
// 	// Benchmark time: 39746 ns/op (2.97x faster than baseline)

// 	// Similar to one, but has a special case for f=2 then increments by two at a time.
// 	// Also quickly reduces something that can be factored by x^n e.g. 6144 can be factored by 2^11 to leave 3

// 	// Run Two
// 	res := Two(600851475143)

// 	// ===============
// 	// v3
// 	// Benchmark time: 9184 ns/op

// 	// Improves on two; overview doc says the following:
// 	// key realisation: every number n can at most have one prime factor greater than n . If we,
// 	// after dividing out some prime factor, calculate the square root of the remaining number we
// 	// can use that square root as upper limit for factor.

// 	// Run Three
// 	// res := largestPrimeFactorThree(14641)

// 	fmt.Printf("Result is %d\n", res)

// }
