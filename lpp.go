package euler

import (
	"math"
)

// Reverse reverses a given integer
func Reverse(x int) int {
	out := 0
	for {
		if x == 0 {
			break
		}
		out = out*10 + x%10
		x /= 10
	}
	return out

}

/* LPP Optimisations:

  W WW WXW WXXW WXYXW WXYYXW WXYZYXW WXYZZYXW
  1 11 111 1111 11111 111111 1111111 11111111
W 1 11 101 1001 10001 100001 1000001 10000001
X 0 00 010 0110 01010 010010 0100010 01000010
Y 0 00 000 0000 00100 001100 0010100 00100100
Z 0 00 000 0000 00000 000000 0001000 00011000

Digits:                                    | HCF:
1: w                                       | 1
2: 11w (steps of 11)                       | 11
3: 101w      + 10x                         | 1
4: 1001w     + 110x                        | 11
5: 10001w    + 1010x    + 100y             | 1
6: 100001w   + 10010x   + 1100y            | 11
7: 1000001w  + 100010x  + 10100y  + 1000z  | 1
8: 10000001w + 1000010x + 100100y + 11000z | 11

We can see that for any given palindrome, if the
answer is an even length of digits, one of the
divisors must be divisible by 11.

By this logic, if we are wanting a product from
three digits
*/

// LargestPalindromicProduct finds the highest palindromic product from
// multiplying two numbers of n length together.
func LargestPalindromicProduct(n int) int {

	if n == 0 {
		return 0
	}

	if n == 1 {
		return 9
	}

	// Calculate bounds
	lb := int(math.Pow10(n - 1))
	ub := int(math.Pow10(n) - 1)
	j := ub
	// fmt.Printf("n=%d lb=%d ub=%d\n", n, lb, ub)

	var highest int

	for i := ub; i >= lb; {

		var stepI = 1
		var stepJ = 1
		// Odd lengths only
		if n%2 != 0 {

			// i is divisible by 11!
			if i%11 == 0 {
				stepI = 11
			} else {
				stepJ = 11
				j = ub - ub%11
			}

		}

		for j >= lb {

			r := j * i

			if r <= highest {
				break
			}

			if r == Reverse(r) && r > highest {
				// fmt.Printf("Highest found so far: %d * %d = %d\n", i, j, r)
				highest = r
			}

			j -= stepJ
		}
		i -= stepI
	}

	return highest
}
