package euler

import (
	"testing"
)

var numberToFactor = 600851475143

var lpfTests = []struct {
	n  int
	lf int
}{
	{1, 1},
	{2, 2},
	{13195, 29},
	{600851475143, 6857},
}

func TestLPF(t *testing.T) {

	for _, tt := range lpfTests {
		got := LargestPrimeFactor(tt.n)
		if got != tt.lf {
			t.Errorf("LargestPrimeFactor(%d) = %d, want %d", tt.n, got, tt.lf)
		}
	}

	// t.Run("One", func(t *testing.T) {
	// 	for _, tt := range lpfTests {
	// 		got := One(tt.n)
	// 		if got != tt.lf {
	// 			t.Errorf("One(%d) = %d, want %d", tt.n, got, tt.lf)
	// 		}
	// 	}
	// })

	// t.Run("Two", func(t *testing.T) {
	// 	for _, tt := range lpfTests {
	// 		got := Two(tt.n)
	// 		if got != tt.lf {
	// 			t.Errorf("Two(%d) = %d, want %d", tt.n, got, tt.lf)
	// 		}
	// 	}
	// })

	// t.Run("Three", func(t *testing.T) {
	// 	for _, tt := range lpfTests {
	// 		got := Three(tt.n)
	// 		if got != tt.lf {
	// 			t.Errorf("Three(%d) = %d, want %d", tt.n, got, tt.lf)
	// 		}
	// 	}
	// })
}

func BenchmarkLPF(b *testing.B) {

	for i := 0; i < b.N; i++ {
		LargestPrimeFactor(numberToFactor)
	}

	// b.Run("One", func(b *testing.B) {
	// 	for i := 0; i < b.N; i++ {
	// 		One(numberToFactor)
	// 	}
	// })

	// b.Run("Two", func(b *testing.B) {
	// 	for i := 0; i < b.N; i++ {
	// 		Two(numberToFactor)
	// 	}
	// })

	// b.Run("Three", func(b *testing.B) {
	// 	for i := 0; i < b.N; i++ {
	// 		Three(numberToFactor)
	// 	}
	// })

}
