package euler

import "errors"

// absInt returns the absolute value of a given integer
func absInt(n int) int {
	if n < 0 {
		return -1 * n
	}
	return n
}

// LowestCommonMultiple takes two numbers and returns their lowest common multiple
func LowestCommonMultiple(a int, b int) (int, error) {

	if a == 0 && b == 0 {

		return 0, errors.New("euler: LCM of 0 and 0 causes division by zero")

	} else if a == 0 || b == 0 {

		return 0, nil

	}

	hcf, err := HighestCommonFactor(a, b)
	if err != nil {
		return 0, err
	}

	return absInt(a*b) / hcf, nil

}

// LCMGroup takes a slice of integers and returns the lowest common multiple of
// all integers in that slice.
func LCMGroup(n []int) (int, error) {
	a := n[0]
	for _, b := range n[1:] {
		var err error
		a, err = HighestCommonFactor(a, b)
		if err != nil {
			return 0, err
		}
	}
	return a, nil
}
